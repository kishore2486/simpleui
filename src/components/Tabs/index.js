import React from 'react';
import { StyledTabs } from '../Styled';
import Login from '../Login';
import SignUp from '../SignUp';

class Tabs extends React.Component {
    state = {
        comp: null
    }

    handleLogin = () => {
        this.setState({
            comp: [<Login />, 'login']
        })
    }

    handleSignUp = () => {
        this.setState({
            comp: [<SignUp />, 'signup']
        })
    }

    render() {
        const { comp } = this.state;
        let style = { backgroundColor: 'mediumseagreen', color: 'white' };
        return (
            <StyledTabs>
                <ul>
                    <li onClick={this.handleLogin} style={comp && comp[1] === 'login' ? style : null}>Login</li>
                    <li onClick={this.handleSignUp} style={comp && comp[1] === 'signup' ? style : null}>SignUP</li>
                </ul>
                {comp && comp[0]}
            </StyledTabs>
        )
    }
}

export default Tabs;
import styled from 'styled-components';

export const StyledTabs = styled.div`
  width: 100%;
  padding: 8px 15px;
  margin: 0 auto;
  background-color: orange;


  ul {
    display: flex;
    flex-direction: row;
    align-items: center;
    justify-content: space-around;
    margin-top: 30px;
    letter-spacing: 0.5px;
    margin-bottom: 30px;
    text-align: center;
  }

  ul li {
    width: 50%;
    padding: 8px 0;
    font-size: 20px;
  }

  ul li {
    list-style-type: none;
  }
`;

export const FormButton = styled.button`
    margin-top: .25rem;
    margin-bottom: .25rem;
    color: #fff;
    width: 20%;
    background-color: #28a745;
    border-color: #28a745;
    border: 1px solid transparent;
    padding: .375rem .75rem;
    font-size: 1rem;
    line-height: 1.5;
    border-radius: .25rem;
    transition: color .15s ease-in-out,background-color .15s ease-in-out,border-color .15s ease-in-out,box-shadow .15s ease-in-out;
`; 
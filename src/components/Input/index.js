import React from 'react';
import PropTypes from 'prop-types';
import styles from './index.module.css';

const Input = ({ label, htmlFor, type, placeholder, error, change }) => {
    const InputClassName = error ? `${styles.isInvalid}` : null;
    return (
        <div className={styles.formGroup}>
            <label htmlFor={htmlFor}>{label}</label>
            <input
                type={type}
                className={`${styles.formControl} ${InputClassName}`}
                id={htmlFor}
                placeholder={placeholder}
                onChange={change}
            />
            {error && <div className={styles.invalidFeedback}>{error}</div>}
        </div>
    );
};

Input.propTypes = {
    label: PropTypes.string.isRequired,
    htmlFor: PropTypes.string.isRequired,
    type: PropTypes.string.isRequired,
    placeholder: PropTypes.string.isRequired,
    change: PropTypes.func.isRequired,
    error: PropTypes.string.isRequired,
};

export default Input;

import React from 'react';
import Input from '../Input';
import { FormButton } from '../Styled';

class SignUP extends React.Component {
    state = {
        email: '',
        emailError: false,
        password: '',
        passwordError: false,
        conpasswordError: false,
        mobileNoError: false,
        mobileNo: null
    };

    shouldComponentUpdate(nextProps, nextState) {
        const { emailError, passwordError } = this.state;
        return (
            nextState.emailError !== emailError ||
            nextState.passwordError !== passwordError
        );
    }

    handleEmail = e => {
        return /[A-Z0-9._%+-]+@[A-Z0-9.-]+.[A-Z]{2,4}/gim.test(e.target.value)
            ? this.setState({
                emailError: false,
                email: e.target.value,
            })
            : this.setState({
                emailError: true,
            });
    };

    handlePassword = e => {
        return e.target.value.length >= 8 && e.target.value.length <= 16
            ? this.setState({
                passwordError: false,
                password: e.target.value,
            })
            : this.setState({
                passwordError: true,
            });
    };
    
    handleMobileNo = e => {
      return e.target.value.length === 10 ? this.setState({
          mobileNoError: false,
          mobileNo: e.target.value
          })
             : this.setState({
                mobileNoError: true
            })
    }

    confirmPassword = e => {
        const { password } = this.state;
        return password === e.target.value ? this.setState({
            conpasswordError: false,
        })
            : this.setState({
                conpasswordError: true
            })
    }

    render() {
        const { email, emailError, password, passwordError, mobileNo conpasswordError, mobileNoError } = this.state;
        return (
            <form>
                <Input
                    label="Email"
                    htmlFor="email"
                    type="email"
                    placeholder="Enter your email"
                    value={email}
                    change={this.handleEmail}
                    error={emailError ? 'Enter your valid email address' : ''}
                />
                <Input
                    label="Mobile"
                    htmlFor="mobile"
                    type="number"
                    placeholder="Enter your Mobile No"
                    value={email}
                    change={this.handleMobileNo}
                    error={mobileNoError ? 'Enter your 10 digit mobile number' : ''}
                />
                <Input
                    label="Password"
                    htmlFor="password"
                    type="password"
                    placeholder="Enter your password"
                    value={password}
                    change={this.handlePassword}
                    error={passwordError ? 'Password must be 8 to 10 characters' : ''}
                />
                <Input
                    label="Confirm Password"
                    htmlFor="conpassword"
                    type="password"
                    placeholder="Enter your password again"
                    value={password}
                    change={this.confirmPassword}
                    error={conpasswordError ? 'Password doesn\'t match' : ''}
                />
                <div style={{ display: 'flex', justifyContent: 'center' }}>
                    <FormButton>SignUP</FormButton>
                </div>
            </form>
        );
    }
}


export default SignUP;

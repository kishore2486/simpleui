import React from 'react';
import Input from '../Input';
import { FormButton } from '../Styled';

class Login extends React.Component {
    state = {
        email: '',
        emailError: false,
        password: '',
        passwordError: false,
    };

    shouldComponentUpdate(nextProps, nextState) {
        const { emailError, passwordError } = this.state;
        return (
            nextState.emailError !== emailError ||
            nextState.passwordError !== passwordError
        );
    }

    handleEmail = e => {
        return /[A-Z0-9._%+-]+@[A-Z0-9.-]+.[A-Z]{2,4}/gim.test(e.target.value)
            ? this.setState({
                emailError: false,
                email: e.target.value,
            })
            : this.setState({
                emailError: true,
            });
    };

    handlePassword = e => {
        return e.target.value.length >= 8 && e.target.value.length <= 16
            ? this.setState({
                passwordError: false,
                password: e.target.value,
            })
            : this.setState({
                passwordError: true,
            });
    };

    render() {
        const { email, emailError, password, passwordError } = this.state;
        return (
            <form>
                <Input
                    label="Email"
                    htmlFor="email"
                    type="email"
                    placeholder="Enter your email"
                    value={email}
                    change={this.handleEmail}
                    error={emailError ? 'Enter your valid email address' : ''}
                />
                <Input
                    label="Password"
                    htmlFor="password"
                    type="password"
                    placeholder="Enter your password"
                    value={password}
                    change={this.handlePassword}
                    error={passwordError ? 'Password must be 8 to 10 characters' : ''}
                />
                <div style={{ display: 'flex', justifyContent: 'center' }}>
                    <FormButton>Login</FormButton>
                </div>
            </form>
        )
    }
}

export default Login;